const express = require('express');
const bodyParser = require('body-parser');
const external_response = require('../../network/response');
const controller = require('./controller');
const router = express.Router();

router.get('/', function(request,response)
    {
        /*
        console.log(request.query);
        console.log(request.body);
        console.log(request.body.ap);
        */

        controller.getMessages()
        .then((messageList) => 
        {
            external_response.success(request,response,messageList,200);
        })
        .catch(e => 
        {
            external_response.error(request,response,'Unexpected error',500,e);
        })
        //response.status(201).send([{error: '', body: 'Creado correctamente', ap: request.body.ap}]);   
    }
);

router.post('/', function(request,response)
    {
        //console.log(request.headers);

        controller.addMessage(request.body.user,request.body.message)
        .then((fullMessage) => 
        {
            external_response.success(request,response,fullMessage,201);
        })
        .catch(e => 
        {
            external_response.error(request,response,'Datos incorrectos',400,e);
        })

        /*
        response.header(
            {
                "custom-header": "mensaje loco personalizado",
            }
        );
        */
        
        //response.send("Info posteada");
    }
);

router.delete('/', function(request,response)
    {
        if(request.query.error == 'ok')
        {
            external_response.error(request,response,'Bailó bertha',401, 'Detalles discretos que no salen al público');
        }
        else
        {
            external_response.success(request,response,'Eliminado');
        }
        //response.status().send();
    }
);

module.exports = router;